// tippy demo
tippy('[data-tippy-content]', {
    trigger: 'mouseenter',
    appendTo: document.getElementsByClassName("wrapper")[0],
    interactive: false,
    arrow: false,
    maxWidth: 260,
    allowHTML: true
});


// Show NFT JSON code  --------------------------------------------------
// var jsonToggleBtn  = document.getElementById("btn-json-toggle");
// var jsonCodeBlock = document.getElementsByClassName("json-code")[0];

// if (jsonToggleBtn) {
//     jsonToggleBtn.addEventListener("click", function(){
//         jsonCodeBlock.classList.toggle("show");
//     }, false);
// }




// Accordion -------------------------------------------------- 

  var cWrapToggle = document.getElementsByClassName("c-wrap__toggle");
  var i;
  
  for (i = 0; i < cWrapToggle.length; i++) {

    cWrapToggle[i].addEventListener("click", function() {
      this.classList.toggle("active");
    });
  }

// Scoll to -------------------------------------------------- 
var btnCurrentStakes = document.querySelector("#btn-current-stakes");

if(btnCurrentStakes) {
  btnCurrentStakes.addEventListener("click", function() {
    var element = document.querySelector("#current-stakes");
    element.scrollIntoView();
  }, false);
}


